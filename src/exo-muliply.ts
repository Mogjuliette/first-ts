/* Afficher en console les tables de multiplication de 1 à 10 (en utilisant des boucles imbriquées)
1. On peut faire ça dans un autre fichier, exo-multiply.ts/html
2. Commencer par afficher déjà une table de multiplication, donc on crée une variable avec un nombre, et ensuite on fait une boucle qui va multiplier à chaque tour
3. Ptit truc en plus, essayer de faire en sorte d'afficher tous les résultats de multiplication sur une seule ligne (on peut par exemple créer une variable de type texte et concaténer les résultats dedans à chaque tour et faire un seul console log à la fin de la boucle)
4. Maintenant qu'on a notre table pour un seul nombre, l'idée va être de faire qu'au lieu d'avoir un seul nombre dans la variable, on ait tous les nombres de 1 à 10 les uns après les autres, chose qu'on peut faire... avec une boucle (on aura donc une première boucle qui contiendra tout le code qu'on a fait dans les étapes 2 et 3) 
 */


let resultat:number
let texte:string =" "

for (let multiply = 1 ; multiply <= 10 ; multiply++){

for (let i = 1; i <= 10 ; i++){
resultat = i*multiply
texte += " "+resultat
}
console.log("Table de " +multiply+" : "+texte)
texte = " "
}