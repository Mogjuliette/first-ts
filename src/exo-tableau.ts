let pays = ["France","Canada","Nepal","Nouvelle-Zélande","Tchad","Uruguay","Lituanie","Argentine","Bénin","Indonésie"]

console.log(pays)

//pop permet d'enlever le dernier élément du tableau
pays.pop()
console.log(pays)

//shift permet d'enlever le premier élément du tableau
pays.shift()
console.log(pays)

//unshift permet de rajouter un élément au début du tableau
pays.unshift("Mexique")
console.log(pays)

//indexOf permet de donner l'index d'un élément du tableau
console.log(pays.indexOf("Nepal"))

//splice permet d'enlever et ou de remplacer un ou plusieurs éléments du tableau
pays.splice(2,0,"Japon") //on ajoute l'élément Japon à partir de l'index 2
console.log(pays)
pays.splice(6,1,"Suisse")// on remplace l'élément d'index 6 par Suisse
console.log(pays)

//slice permet d'afficher une portion du tableau
console.log(pays.slice(2,7,))//affiche les éléments d'index 2 à 6, l'index de fin est exclus

//push permet d'ajouter un ou plusieurs éléments à la fin du tableau
pays.push("Madagascar","Liban")
console.log(pays)

//forof pour faire la liste d'items d'un tableau un par un
for (const state of pays) {
    console.log(state);
}

//forin pour faire la liste d'index d'un tableau un par un
for (const key in pays) {
    console.log(key);
}

//foreach, même chose que le forof, mais la manière d'écrire est différentes, ici on a une fonction fléchée
pays.forEach(element => {
    console.log(element);
});

//filter, permet de filtrer les résultats en fonction d'un critère

let result = pays.filter(mot =>mot.lenght > 4)
console.log(result)

//autre utilisation, avec des nombres
let notes:number[] = [10, 8, 7, 16, 10, 13, 11, 5, 15]
let resultMarks = notes.filter(mark => mark >= 10)
console.log(resultMarks)