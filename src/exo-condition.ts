//1. Créer un nouveau fichier exo-condition.html et un fichier exo-condition.ts et les lier ensemble (avec parcel quand on crée un nouveau html, il faut arrêter le npm start avec ctrl+c et le relancer)
//2. Dans le fichier ts, créer 2 variable, age et isOfAge et faire une condition qui va faire que si age est supérieur à 18, alors on assigne true à isOfAge//
//3. En dessous, faire une autre condition qui va vérifier si isOfAge est true, et si oui, on affiche un message genre 'Welcome to the tax paying app' et sinon on affiche 'Go home kiddo'//

let age:number = 14 ;
let isOfAge:boolean = false ;

if (age >= 18) {
    isOfAge = true ;
}

if (isOfAge == true) {
    console.log ("Welcome to the tax paying app");
} else {
    console.log ("Go home kiddo");
}



/* Exercice calcul :
1. A la suite du fichier exo-condition.ts, créer 2 nouvelles variables a et b qui contiendront n'importe quoi comme nombre (genre 3 et 5)
2. Créer une troisième variable operator qui contiendra une chaîne de caractère
3. Faire plusieurs conditions qui vont vérifier : si operator vaut '+' alors on additionne a et b et on affiche le résultat, si ça contient '-' soustraction, '/' division et '*' multiplication
4. Si on a aucun des opérateurs possibles, on affiche un message 'incorrect operator' avec un console.log
5. à la place de mettre directement une valeur dans operator, faire en sorte de lui assigner un prompt('choose an operator') qui affichera un popup permettant à l'utilisateur·ice d'entrer un opérateur directement */

let a:number = 3;
let b:number = 5;
let operator = prompt('choose an operator', '');
let result:number = 0;

if (operator == "+") {
    console.log(a+b)
} else if (operator == "-") {
    console.log(a-b)
} else if (operator == "/") {
    console.log(a/b)
} else if (operator == "*") {
    console.log(a*b)
} else { console.log("incorrect operator")}

/* switch (operator) {
    case value"+":
        result = a + b
        break;

    default:
        break;
} */