/* 1. Créer un nouveau point d'entrée exo-pyramid.html / exo-pyramid.ts

2. Pour commencer, on peut essayer de faire une première boucle pour avoir autant de console log qu'on a d'étage à la pyramide, donc ici 5 (peu importe ce qu'on console log)

3. Ensuite, on peut essayer de trouver un calcul très savant pour afficher à chaque tour de boucle le nombre d'étoile par étage de la pyramide, donc genre ça comme résultat :

4. Une fois qu'on a le bon nombre d'étage et une variable qui contient le nombre d'étoile à afficher par étage, "ya pu qu'à" faire en sorte d'afficher les étoiles en questions (par exemple avec une boucle qui concatène dans une phrase, comme à l'exo d'avant, sauf que cette boucle sera imbriquée dans l'autre)
Résultat :

5. Ensuite on recommence un peu la même chose qu'à l'étape 3, mais cette fois ci pour le nombre d'espace avant chaque étoile pour avoir un truc qui ressemble à ça :

6. Enfin, on fait comme à l'étape 4 pour concaténer les espaces avant les étoiles, et on obtient notre pyramide

Bonus : Pouvoir changer le nombre d'étage avec une variable et pourquoi pas aussi le caractère (pour pouvoir faire une pyramide de coeurs par exemple <3)  */


let spaceNumber = 0;
let symbol = prompt('Indiquez un symbole');
let floor = prompt('Indiquez le nombre d étages');
let floorNumber:number = Number(floor)*2

for (let i = 1 ; i <= floorNumber ; i = i+2){  
spaceNumber = floorNumber-i/2
let star = " " ;
    
    for (let k = 1; k <= spaceNumber ; k++){
        star += " ";
    }

    for (let j = 1; j <= i ; j++){
        star += symbol ;
    }

console.log(star);

}
