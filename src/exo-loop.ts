/* Boucle où qu'on choisi combien de tour elle fait

1. Dans un fichier exo-loop.ts (lié à un exo-loop.html). Créer une variable loopNumber et mettre dedans le résultat d'un prompt (converti en Number)
2. Créer une boucle for ou while, comme vous voulez, qui va faire le nombre de tour indiqué par loopNumber
3. Dans cette boucle, faire un console log de coucou ou autre, osef (modifié)
*/

/* let loopNumber = Number(prompt("entrez un nombre entier"))

for (let i = 0; i < loopNumber; i++){
    console.log("coucou")
} */


/* Boucler sur les lettres d'un prénom
1. Dans le même fichier (ou un autre peu importe), créer une variable name qui va contenir un prompt
2. Faire un console.log du nombre de lettre qu'il y a dans ce prénom
3. Faire une boucle qui fera autant de tour qu'il y a de lettre dans le prénom 
4. A chaque tour de boucle, essayer d'afficher quelle lettre du prénom se trouve à la position actuelle de la boucle */

let nom:string = prompt ("donnez un prénom")
let letterNumber = nom.length
let result:string = " "

console.log("le nombre de lettres du prénom est " +letterNumber)

for (let i = 0; i < letterNumber; i++)
{
/*     console.log("letter") */
    result = nom.charAt(i);
    console.log(result)
}





