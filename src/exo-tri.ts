/* En partant d'un tableau qui ressemble par exemple à ça : [3,123,4,20,40,342,45,23,64,65,8,1,3,5,6,3]
Créer une fonction de tri qui implémentera l'algorithme bubble sort, vous pouvez voir le principe de cet algo sur wikipédia : https://en.wikipedia.org/wiki/Bubble_sort (vous avez aussi du pseudo code, mais l'idée c'est d'essayer de le faire soit même autant que possible, pasqu'en vrai des implémentation de ces trucs se trouvent très facilement sur internet)
Vous pouvez faire une autre fonction qui implémentera l'algorithme quicksort (https://en.wikipedia.org/wiki/Quicksort) */


let firstArray = [3,123,4,20,40,342,45,23,64,65,8,1,3,5,6,3]
let temporary:number = 0
let swapped:boolean = true

console.log("série d'origine : "+firstArray);

function bubble() {
    while (swapped == true){
        swapped = false;
        for (let i = 0 ; i < firstArray.length ; i++){
            if (firstArray[i] > firstArray[i + 1]){
                temporary = firstArray[i];
                firstArray[i] = firstArray[i+1];
                firstArray[i+1] = temporary;
                swapped = true ;
                }
            } 
    console.log("série en cours de tri : "+firstArray);
   } 
console.log("fin du tri");
} 

bubble()

//autre solution, avec sort :     
    
/* firstArray.sort((a, b) => a - b);
console.log(firstArray); */

