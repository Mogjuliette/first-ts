let tableau:string[] = ["jaune","rouge","bleu","vert","violet"];
let tableauBis:string[] = ["koala","manchot","renard","chat"];
let selectedArray: any[] = [];

console.log("début");

/**
 * Recherche une valeur au sein d'un tableau
 * @param a tableau dans lequel on cherche
 * @param b mot recherché
 * @returns true si mot trouvé, false sinon
 */
function search(a:string[],b:string|null):boolean{
    for (let item of a) {
        if (item == b){
            return true;
        }
    }
    return false;
}

let word:string|null = prompt("Indiquez l'animal recherché")
let resultat = search(tableauBis,word)
console.log(resultat);

word = prompt("Indiquez la couleur recherchée")
resultat = search(tableau,word)
console.log(resultat);
